#!/bin/bash
/home/steam/steamcmd/steamcmd.sh \
    +@sSteamCmdForcePlatformType windows \
    +login anonymous \
    +force_install_dir ${STEAM_APP_NAME} \
    +app_update ${STEAM_APP_ID} validate \
    +quit
