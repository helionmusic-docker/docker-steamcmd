FROM cm2network/steamcmd:latest

USER root
WORKDIR /home/scripts/

RUN apt-get update && apt-get install -y git

COPY scripts/steamcmd-install-app.sh install-app.sh
RUN chmod +x install-app.sh
RUN echo "alias install-app=/home/scripts/install-app.sh" >> ~/.bash_profile

WORKDIR /home/steam/steamcmd

ENTRYPOINT [ "" ]
